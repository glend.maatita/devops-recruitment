FROM node:latest

WORKDIR /app
COPY package.json .

COPY yarn.lock .
RUN yarn

EXPOSE 8080
CMD [ "npm", "start" ]